﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class CharacterContr : MonoBehaviour {

	public float speed = 10.0f;

	public LayerMask groundLayers;

	public float jumpForce = 7;
	private Rigidbody rb;
	public CapsuleCollider col; 


	void Awake() {
		Globals.Instance.Player = this.gameObject;
	}

	// Use this for initialization
	void Start() {

		Cursor.lockState = CursorLockMode.Locked;
	    rb = GetComponent<Rigidbody>();
		col = GetComponent<CapsuleCollider>(); 
		Debug.Log(Globals.Instance.Interactables[0].transform.position);
	}
	
	// Update is called once per frame
	void Update() {

		float translation = Input.GetAxis("Vertical") * speed;
		float strafe = Input.GetAxis("Horizontal") * speed;
		translation *= Time.deltaTime;
		strafe *= Time.deltaTime;
		transform.Translate(strafe, 0, translation);

		if (isGrounded() && Input.GetKeyDown(KeyCode.Space)) {

			rb.AddForce (Vector3.up * jumpForce, ForceMode.Impulse);
		}

		if (Input.GetKeyDown("escape"))
			Cursor.lockState = CursorLockMode.None;

		
	}


	private bool isGrounded(){
		return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,col.bounds.min.y,col.bounds.center.z), col.radius* 0.9f, groundLayers);

	}

}
