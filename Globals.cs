﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Globals : Singleton<Globals>
{

    // (Optional) Prevent non-singleton constructor use.
    protected Globals() { }
 
    // Then add whatever code to the class you need as you normally would.
    public string MyTestString = "Hello world!";

    public List<GameObject> Interactables = new List<GameObject>();

    public GameObject Player;


}
