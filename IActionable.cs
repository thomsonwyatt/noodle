﻿public interface IActionable
{
    void OnAction();
}
