﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    GameObject Player;
    // Start is called before the first frame update
    void Awake(){
        Globals.Instance.Interactables.Add(this.gameObject);
    }

    void Start()
    {
        Player = Globals.Instance.Player;
        Debug.Log(Player.ToString());
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(this.gameObject.transform.position, Player.transform.position) < 2){
            Debug.Log("You're close to me!");
            this.gameObject.SendMessage("OnAction");
        }
    }
}
