﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestObject : MonoBehaviour, IActionable
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnAction()
    {
        if(Input.GetKeyDown(KeyCode.J)){
            Debug.Log("Jump");
        }
    }
}
